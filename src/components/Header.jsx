import React from "react";
import {PhoneIcon} from '@heroicons/react/24/outline'





function Header (props) {
    return(
        <header className="lg:container lg:mx-auto  bg-yellow-300 shadow-lg">
            <div className="mx-2 lg:mx-8 h-24 flex flex-row justify-between items-center ">
              <span className="text-sm lg:text-lg font-semibold text-fuchsia-800	">300 Bowling Lane</span>
                <ul>
                    <li className="flex flex-row gap-1 lg:gap-4 font-semibold">
                        <PhoneIcon className="w-4 lg:w-6 h-4 lg:h-6"/>
                        <span className="text-xs lg:text-base">3035186112</span>
                        <span className="text-xs lg:text-base"> 3034186172</span>
                    </li>
                    <li>
                        <span className="mt-2 text-xs lg:text-xs hidden lg:block">Please call us between 10:00 Am - 10:00 PM</span>
                    </li>
                    <li></li>
                </ul>
            </div>
            
        </header>
    )
}

export default Header