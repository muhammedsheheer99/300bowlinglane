import React, { Component } from "react";
import Slider from "react-slick";
import Slide1 from "../Slides/Slide1";
import Slide2 from "../Slides/Slide2";
import Slide3 from "../Slides/Slide3";
import Slide4 from "../Slides/Slide4";
import Slide5 from "../Slides/Slide5";

export default class MySlider extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      speed: 1500,
      autoplaySpeed: 3000,
      cssEase: "linear"
    };
    return (
      <div>
        <Slider {...settings}>
          <Slide1/>
          <Slide2/>
          <Slide3/>
          <Slide4/>
          <Slide5/>
        </Slider>
      </div>
    );
  }
}
