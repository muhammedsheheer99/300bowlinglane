import react from "@heroicons/react";
import {PhoneIcon} from '@heroicons/react/24/outline'


function Footer (){
    return(
        <div className="py-6 bg-red-300 h-40 lg:container lg:mx-auto  lg:px-8 flex flex-col lg:flex-row justify-between items-center">
            <div>
            <span className="text-sm lg:text-lg font-semibold text-fuchsia-800	">300 Bowling Lane</span>
            </div>
            <div>
            <ul className="flex flex-row gap-2">
                <li>
                    <img src="images/instagram.svg" alt="" />
                </li>
                <li>
                    <img src="images/facebook.svg" alt="" />
                </li>
                <li>
                    <img src="images/pinterest.svg" alt="" />
                </li>
            </ul>
            </div>
            <div>
            <ul>
                    <li className="flex flex-row gap-4 font-semibold">
                        <PhoneIcon className="w-4 lg:w-6 h-4 lg:h-6"/>
                        <span className="text-xs lg:text-base">3035186112</span>
                        <span className="text-xs lg:text-base"> 3034186172</span>
                    </li>
                    <li>
                        <span className="mt-2 text-xs lg:text-xs hidden lg:block">Please call us between 10:00 Am - 10:00 PM</span>
                        <span className="mt-2 text-xs lg:text-xs block lg:hidden text-center ">Please call us between <br /> 10:00 Am - 10:00 PM</span>
                    </li>
                    <li></li>
                </ul>
            </div>
        </div>
        
    )
}

export default Footer