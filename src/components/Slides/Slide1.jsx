import react from "@heroicons/react";

function Slide1 (){
    return(
        <div className="lg:container lg:mx-auto flex flex-row justify-center">
            <div className="w-full h-96 lg:h-[35rem] flex flex-col justify-end lg:justify-center gap-2 lg:gap-4  bg-cover bg-center bg-[url('/images/Tennis.jpg')]">
                <span className="ml-8 text-white text-lg lg:text-4xl fond-md">50 QAR /60 min</span>
                <span className="ml-8 text-white text-lg lg:text-4xl fond-md">30 QAR /30 min</span>
            </div>
        </div>
    )
}

export default Slide1