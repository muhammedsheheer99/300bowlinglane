import react from "@heroicons/react";

function Slide3 (){
    return(
        <div className="container mx-auto flex flex-row justify-center">
            <div className="w-full h-96 lg:h-[35rem] flex flex-col justify-end lg:justify-center gap-2 lg:gap-4 bg-cover bg-center bg-[url('/images/babyfoot.jpg')]">
                <span className="ml-8 text-white text-lg lg:text-4xl fond-md">25 QAR /60 min</span>
                <span className="ml-8 text-white text-lg lg:text-4xl fond-md">15 QAR /30 min</span>
            </div>
        </div>
    )
}

export default Slide3