import react from "@heroicons/react";

function Slide5 (){
    return(
        <div className="container mx-auto flex flex-row justify-center">
            <div className="w-full h-96 lg:h-[35rem] flex flex-col justify-end lg:justify-center gap-1 lg:gap-2 bg-cover bg-center bg-[url('/images/bowling.jpg')]">
                <span className="ml-8 text-white text-lg lg:text-4xl fond-md">15 QAR /Per Game</span>
                <span className="ml-8 text-white text-sm lg:text-2xl fond-md">3 QAR Shoes</span>
                <span className="ml-8 text-white text-sm lg:text-2xl fond-md">3 QAR Socks</span>
            </div>
        </div>
    )
}

export default Slide5