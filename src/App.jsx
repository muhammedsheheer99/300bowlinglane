import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import './App.css'
import Header from './components/Header'
import Slider from 'react-slick';
import MySlider from './components/Slider/Slider';
import Footer from './components/Footer';

function App() {
  

  return (
    <>
      <Header/>
      <main className='container mx-auto'>
        <section>
          <div>
          <div class="flex flex-row items-center w-full h-80 bg-cover bg-center  opacity-85	 bg-[url('/images/300bowling.jpg')]">
            <div className='mx-8 flex flex-col gap-2'>
            <span className='text-white text-3xl font-semibold'>300 Bowling Lane</span>
            <p className='text-yellow-500 text-sm font-semibold'>"Let's Roll the Ball and Strike the Fun!"</p>
            </div>
          </div>
          </div>
        </section>
        <section>
          <MySlider/>
        </section>
      </main>
      <Footer/>
    </>
  )
}

export default App
